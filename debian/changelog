libmath-random-isaac-perl (1.004-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Bump debhelper from old 12 to 13.
  * Apply multi-arch hints. + libmath-random-isaac-perl: Add Multi-Arch: foreign.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 30 Nov 2022 00:00:34 +0000

libmath-random-isaac-perl (1.004-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 25 Dec 2020 18:06:21 +0100

libmath-random-isaac-perl (1.004-1) unstable; urgency=medium

  * Team upload

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Mark package as source format 3.0 (quilt)
  * Update d/copyright to copyright-format 1.0
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4

 -- Florian Schlichting <fsfs@debian.org>  Sun, 01 Jul 2018 23:03:46 +0200

libmath-random-isaac-perl (1.003-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Note that the algorithm was Bob Jenkins, though he's not a copyright
    holder

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Add perl (>= 5.10.1) as an alternative build dependency for
    Pod::Simple.

  [ Dominic Hargreaves ]
  * Note that this new release fixes FTBFS on perl 5.12 (Closes: #578898)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 13 Feb 2011 17:05:46 +0000

libmath-random-isaac-perl (1.001-1) unstable; urgency=low

  * Initial Release (Closes: #525334)

 -- Jonathan Yu <frequency@cpan.org>  Thu, 27 Aug 2009 05:51:07 -0400
